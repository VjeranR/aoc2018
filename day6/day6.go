package day6

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"math"
	"regexp"
	"strconv"
)

type Point struct {
	x int64
	y int64
	name int64 // 0, 1 ...
	area int
	infiniteField bool
}

type Boundaries struct {
	maxX int64
	maxY int64
}

func ChronalCoordinates() {
	//file := helpers.Fread("day6/test_input.txt")
	file := helpers.Fread("day6/input.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)
	
	points := make([]Point, 0)

	var pointName int64 = 0
	for scanner.Scan() {
	    s := scanner.Text()

		userParams := regexp.MustCompile(`(\d+), (\d+)`).FindAllStringSubmatch(s, -1)

		x, _ := strconv.ParseInt(userParams[0][1], 10, 64)
		y, _ := strconv.ParseInt(userParams[0][2], 10, 64)
		
		point := Point{x, y, pointName, 0,false}
		points = append(points, point)

		pointName++
	}

	boundaries := initBoundaries(points)
	grid := initGrid(boundaries)

	// go through all the points in grid and calculate closest distance to a point
	grid = calculateClosestDistance(boundaries, points, grid)

	var i, j int64
	var pointIndex int
	for i = 0; i < boundaries.maxY+1; i++ {
		for j = 0; j < boundaries.maxX+1; j++ {
			pointIndex = grid[i][j]

			if pointIndex != -1 {
				if i == 0 || j == 0 || i == boundaries.maxY || j == boundaries.maxX {
					points[pointIndex].infiniteField = true
				}

				points[pointIndex].area++
			}

		}
	}

	largestArea := 0
	for _, point := range points {
		if point.area > largestArea && point.infiniteField == false {
			largestArea = point.area
		}
	}

	fmt.Println("largest area (part 1)", largestArea)


	/* --------------------------------------------------------- */

	grid = initGrid(boundaries)
	for i = 0; i < boundaries.maxY+1; i++ {
		for j = 0; j < boundaries.maxX+1; j++ {

			var distances []float64
			for k := 0; k < len(points); k++ {
				distance := math.Abs(float64(i-points[k].y)) + math.Abs(float64(j-points[k].x))
				distances = append(distances, distance)
			}

			var sum float64 = 0
			for _, d := range distances {
				sum += d
			}

			if sum < 10000 {
				grid[i][j] = 0
			}
		}
	}

	closestAreaSize := 0
	for i = 0; i < boundaries.maxY+1; i++ {
		for j = 0; j < boundaries.maxX+1; j++ {

			if grid[i][j] == 0 {
				closestAreaSize++
			}
		}
	}

	fmt.Println("part 2", closestAreaSize)
	//for i = 0; i < boundaries.maxY + 1; i++ {
	//	fmt.Println(grid[i])
	//}
}

func calculateClosestDistance(boundaries Boundaries, points []Point, grid [][]int) [][]int {
	var i, j int64
	for i = 0; i < boundaries.maxY+1; i++ {
		for j = 0; j < boundaries.maxX+1; j++ {

			// find closest point
			closestPointIndex := 0
			var minDistance float64 = -1
			var distance float64
			for k := 0; k < len(points); k++ {
				distance = math.Abs(float64(i-points[k].y)) + math.Abs(float64(j-points[k].x))

				if minDistance == -1 {
					minDistance = distance
					closestPointIndex = k
				} else if distance < minDistance {
					minDistance = distance
					closestPointIndex = k
				} else if distance == minDistance {
					closestPointIndex = -1
				}
			}

			grid[i][j] = closestPointIndex
		}
	}

	return grid
}

func initGrid(boundaries Boundaries) [][]int {
	grid := make([][]int, boundaries.maxY + 1)
	for i := 0; i < int(boundaries.maxY) + 1; i++ {
		grid[i] = make([]int, int(boundaries.maxX) + 1)

		for j := 0; j < int(boundaries.maxX) + 1; j++ {
			grid[i][j] = -1
		}
	}
	return grid
}

func initBoundaries(points []Point) Boundaries {
	boundaries := Boundaries{0, 0}
	for _, point := range points {

		if point.x > boundaries.maxX {
			boundaries.maxX = point.x
		}

		if point.y > boundaries.maxY {
			boundaries.maxY = point.y
		}
	}
	return boundaries
}