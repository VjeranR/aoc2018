package day11

import (
	"fmt"
	"strconv"
	"strings"
)

func ChronalCharge() {
	serialNumber := 7315

	grid := initGrid(serialNumber)

	largestSum := 0
	x := 0
	y := 0
	for i := 0; i < len(grid) - 2; i++  { // y
		for j := 0; j < len(grid) - 2; j++ { // x

			sum := 0
			for k := i; k < i + 3; k++ {
				for l := j; l < j + 3; l++ {
					sum += grid[k][l]
				}
			}

			if sum > largestSum {
				largestSum = sum
				x = j
				y = i
			}
		}
	}

	fmt.Println("part 1: sum, x, y", largestSum, x + 1, y + 1)

	/* ---------------------------------------------------------------- */

	largestSum = 0
	x = 0
	y = 0
	bestCellSize := 0
	for size := 300; size > 0; size-- {
		for i := 0; i < len(grid) - size + 1; i++  { // y
			for j := 0; j < len(grid) - size + 1; j++ { // x

				sum := 0
				for k := i; k < i + size; k++ {
					for l := j; l < j + size; l++ {
						sum += grid[k][l]
					}
				}

				if sum > largestSum {
					largestSum = sum
					x = j
					y = i
					bestCellSize = size
				}
			}
		}
	}

	fmt.Println("part 2: sum, size, x, y", largestSum, x + 1, y + 1, bestCellSize)

}

func initGrid(serialNumber int) [][]int {
	grid := make([][]int, 300)
	for y := 0; y < 300; y++ {
		grid[y] = make([]int, 300)

		for x := 0; x < 300; x++ {
			grid[y][x] = calculatePowerLevel(x, y, serialNumber)
		}
	}


	return grid
}

func calculatePowerLevel(x int, y int, serialNumber int) int {

	rackId := (x+1) + 10 	//Find the fuel cell's rack ID, which is its X coordinate plus 10.
	powerLevel := rackId * (y+1) 	//Begin with a power level of the rack ID times the Y coordinate.

	powerLevel += serialNumber 	//	Increase the power level by the value of the grid serial number (your puzzle input).
	powerLevel *= rackId	//Set the power level to itself multiplied by the rack ID.

	getStringDigit := strconv.Itoa(powerLevel)
	charArray := strings.Split(getStringDigit, "")
	for i, j := 0, len(charArray)-1; i < j; i, j = i+1, j-1 { // reverse
		first := charArray[i]
		second := charArray[j]

		charArray[i] = second
		charArray[j] = first
	}

	//	Keep only the hundreds digit of the power level (so 12345 becomes 3; numbers with no hundreds digit become 0).
	var hundredDigit string
	if len(charArray) > 2 {
		hundredDigit = charArray[2]
	} else {
		hundredDigit = "0"
	}

	//Subtract 5 from the power level.
	cellPower, _ := strconv.ParseInt(hundredDigit, 10, 64)
	power := int(cellPower) - 5

	return power
}
