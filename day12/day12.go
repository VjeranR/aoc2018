package day12

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"regexp"
	"strings"
)

func SubterraneanSustainability() {

	file := helpers.Fread("day12/test_input.txt")
	plantState := "..." + "#..#.#..##......###...###" + ".........."
	//plantArray := strings.Split(plantState, "")

	//file := helpers.Fread("day12/input.txt")
	defer file.Close()

	rules := make([]string, 0)

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		s := scanner.Text()

		rules = append(rules, s)
	}

	fmt.Println(plantState)

	for i := 0; i < 20; i++ {
			for _, rule := range rules {

				matches := strings.Split(rule, " => ")

				//fmt.Println("matches", matches)

				// treba zamijeniti samo taj znak, ne povoziti prije promijenjeno
				// možda je to lakše sa bitmaskama
				//ruleSplit := strings.Split(matches[0], "")
				//matchString := ``
				//for _, r := range ruleSplit {
				//	if r == "." {
				//		matchString += `(\.)`
				//	} else {
				//		matchString += `(#)`
				//	}
				//}

				if matches[1] == "#" {
					//re := regexp.MustCompile(matchString)
					//plantState = re.ReplaceAllString(plantState, "$1$2#$4$5")

					plantState = strings.Replace(plantState, matches[0], "..#..", 1)
				} else {
					//re := regexp.MustCompile(`(\.)#(\.)(\.)(\.)`)
					//s := re.ReplaceAllString("...#...#....#.....#...#....#..........", "$1$2#$3$4")

					plantState = strings.Replace(plantState, matches[0], ".....", 1)
				}
			}

			fmt.Println(plantState)
	}

	fmt.Println("...............................")

	re := regexp.MustCompile(`(\.)#(\.)(\.)(\.)`)
	s := re.ReplaceAllString("...#...#....#.....#...#....#..........", "$1$2#$3$4")
	fmt.Println(s)

	//test := "..." + "#..#.#..##......###...###" + ".........."
	//splitted := strings.Split(test, "")
	//
	//sum := 0
	//for i := -3; i < 36 ; i++  {
	//
	//	if splitted[i + 3] == "#" {
	//		sum += i
	//	}
	//}
	//
	//fmt.Println(sum)
}

