package day2

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"strings"
)

func InventoryManagementSystem() {
	file := helpers.Fread("day2/test_input2.txt")
	//file := helpers.Fread("day2/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	checksumMap := make(map[int]string)
	sumTwo := 0; sumThree := 0

	for scanner.Scan() {
		s := scanner.Text()
		letters := 	strings.Split(s, "")

		letterMap := countLetters(letters)
		fmt.Println("letters: ", letterMap, len(letterMap))

		hasTwo := false; hasThree := false
		for key, element := range letterMap {
			switch element {
			case 2:
				hasTwo = true
			case 3:
				hasThree = true
			}

			_, initialized := checksumMap[element]

			if !initialized {
				checksumMap[element] = key
			} else {
				if !strings.Contains(checksumMap[element], key) {
					checksumMap[element] += key
				}
			}
		}

		if hasTwo {
			sumTwo++
		}

		if hasThree {
			sumThree++
		}
	}

	fmt.Println("checksum: ", checksumMap)
	fmt.Println(sumTwo * sumThree)
}

func InventoryManagementSystem_2() {
	//file := helpers.Fread("day2/test_input2.txt")
	file := helpers.Fread("day2/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	var allLetters [250][]string
	i := 0

	for scanner.Scan() {
		s := scanner.Text()
		letters := 	strings.Split(s, "")

		fmt.Println("letters: ", letters)
		allLetters[i] = letters
		i++
	}

	fmt.Println(allLetters)
	answer := ""

	for k := 0; k < len(allLetters); k++ {
		for l := k; l < len(allLetters); l++ {

			diffCount := 0; diff := ""
			for j := 0; j < len(allLetters[k]); j++ {
				if allLetters[k][j] == allLetters[l][j] {
					diff += allLetters[k][j]
				} else {
					diffCount++
				}
			}

			if(diffCount == 1) {
				answer = diff
			}

			fmt.Println("diff", diffCount, diff, answer)
		}

	}

	fmt.Println("answer", answer)
}

func countLetters(letters []string) map[string]int {
	letterMap := make(map[string]int)
	for _, element := range letters {
		_, initialized := letterMap[element]

		if !initialized {
			letterMap[element] = 1
		} else {
			letterMap[element]++
		}
	}
	return letterMap
}
