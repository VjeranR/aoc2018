package day2

import (
	"bufio"
	"fmt"
	"os"
)

func Reddit() {
	fmt.Println("reading...")
	input := read()

	letters := [26]string{"a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"}

	twos, threes := 0, 0
	counts := [26]byte{}
	for _, id := range input {

		counts = [26]byte{}
		for _, r := range id {
			if r < 'a' || r > 'z' {
				panic("not a letter")
			}
			counts[r-'a']++
		}

		fmt.Println(counts)

		has2, has3 := false, false
		for i, c := range counts {
			switch c {
			case 3:
				fmt.Println("3", letters[i])
				has3 = true

			case 2:
				fmt.Println("2", letters[i])
				has2 = true
			}
		}
		if has2 {
			twos++
		}
		if has3 {
			threes++
		}

	}

	fmt.Println("twos threes", twos, threes)
	fmt.Printf("Answer: %d\n", twos*threes)
}

func read() (input []string) {
	s := bufio.NewScanner(os.Stdin)
	for s.Scan() {
		input = append(input, s.Text())
	}
	return input
}