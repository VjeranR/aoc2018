package day8

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"strconv"
	"strings"
)

type Node struct {
	numberOfChildren int64
	metadataLength int64
	children []*Node
	metadata []string
}

func MemoryManeuver() {

	//file := helpers.Fread("day8/test_input.txt")
	file := helpers.Fread("day8/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		input := scanner.Text()
		inputArray := strings.Split(input, " ")

		solution, data := createGraph3(inputArray)

		fmt.Println(solution, data)
	}
}

func createGraph3(inputArray []string) (int64, []string)  {
	numberOfChildren, _ := strconv.ParseInt(inputArray[0], 10, 64)
	metadataLength, _ := strconv.ParseInt(inputArray[1], 10, 64)
	inputArray = inputArray[2:]

	var childSums int64 = 0
	subtreeScores := make([]int64, 0)

	for i := 0; i < int(numberOfChildren); i++ {
		grandchildrenSum, data := createGraph3(inputArray)
		inputArray = data
		childSums += grandchildrenSum
		subtreeScores = append(subtreeScores, grandchildrenSum)
	}

	if numberOfChildren == 0 {
		var leafValue int64 = 0
		metadata := inputArray[:metadataLength]
		inputArray = inputArray[metadataLength:]

		for j := 0; j < int(metadataLength); j++ {
			metaByte, _ := strconv.ParseInt(metadata[j], 10, 64)
			leafValue += metaByte
		}

		return leafValue, inputArray
	} else {
		var childrenValues int64 = 0
		metadata := inputArray[:metadataLength]
		inputArray = inputArray[metadataLength:]

		for j := 0; j < len(metadata); j++ {
			metaByte, _ := strconv.ParseInt(metadata[j], 10, 64)

			//childrenValues += metaByte
			if metaByte > 0 && int(metaByte) <= len(subtreeScores) {
				childrenValues += subtreeScores[metaByte-1]
			}
		}

		//return childSums + childrenValues, inputArray
		return childrenValues, inputArray
	}
}