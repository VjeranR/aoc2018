package day3

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"regexp"
	"strconv"
)

func OverlapingRectangles () {
	//file := helpers.Fread("day3/test_input.txt")
	file := helpers.Fread("day3/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	fabric := make([][]int64, 1000)
	for i := range fabric {
		fabric[i] = make([]int64, 1000)
	}

	notOverlaping := make([]int64, 1308)

	for scanner.Scan() {
		s := scanner.Text()

		id, x, y, width, length := inputLineExtractParameters(s)
		for i := x; i < x + width; i++ {
			for j := y; j < y + length; j++ {
				if fabric[i][j] == 0 {
					fabric[i][j] = id

					if notOverlaping[id] == 0 {
						notOverlaping[id] = 1
					}
				} else {
					notOverlaping[id] = -1

					if fabric[i][j] > 0 {
						notOverlaping[fabric[i][j]] = -1
					}

					fabric[i][j] = -1
				}
			}
		}
	}

	countOverlaps := 0
	for i := 0; i < len(fabric); i++ {
		for j := 0; j < len(fabric[i]); j++ {
			if fabric[i][j] == -1 {
				countOverlaps++
			}
		}
	}

	fmt.Println("overlap numbers", countOverlaps)

	for i := 0; i < len(notOverlaping); i++ {
		if notOverlaping[i] == 1 {
			fmt.Println("not overlaping: ", i)
		}
	}
}

func inputLineExtractParameters(s string) (int64, int64, int64, int64, int64) {
	userParams := regexp.MustCompile("#(\\d+) @ (\\d+),(\\d+): (\\d+)x(\\d+)").FindAllStringSubmatch(s, -1)

	id, _ := strconv.ParseInt(userParams[0][1], 10, 64)
	x, _ := strconv.ParseInt(userParams[0][2], 10, 64)
	y, _ := strconv.ParseInt(userParams[0][3], 10, 64)
	width, _ := strconv.ParseInt(userParams[0][4], 10, 64)
	length, _ := strconv.ParseInt(userParams[0][5], 10, 64)

	return id, x, y, width, length
}
