package day10

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"math"
	"regexp"
	"strconv"
	"strings"
)

type Point struct {
	X int64
	Y int64
	vX int64
	vY int64
}

func Sum(x int, y int) int {
	return x + y
}

// vjerojatno je rješenje pratit točke prije i poslije izvođenja jedne sekunde
// ako je udaljenost između min i max veća od prošlog isprintaj prošlo stanje točaka tj poruku

func TheStarsAlign() {
	//file := helpers.Fread("day10/test_input.txt")
	file := helpers.Fread("day10/input.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)

	points := make([]Point, 0)

	var X, Y, vX, vY int64
	var point Point

	for scanner.Scan() {
	    s := scanner.Text()

		userParams := regexp.MustCompile(`position=<([-|\s]*?\d+), ([-|\s]*?\d+)> velocity=<([-|\s]*?\d+), ([-|\s]*?\d+)>`).FindAllStringSubmatch(s, -1)

		X, _ = strconv.ParseInt(strings.TrimSpace(userParams[0][1]), 10, 64)
		Y, _ = strconv.ParseInt(strings.TrimSpace(userParams[0][2]), 10, 64)
		vX, _ = strconv.ParseInt(strings.TrimSpace(userParams[0][3]), 10, 64)
		vY, _ = strconv.ParseInt(strings.TrimSpace(userParams[0][4]), 10, 64)

		point = Point{X, Y, vX, vY}

		points = append(points, point)
	}


	skyCoordinates := make(map[int64]map[int64]string, 0)
	var distance float64 = 0

	breakExec := false
	for second := 0; second <= 15000; second++ {
		skyCoordinates = make(map[int64]map[int64]string, 0)
		skyCoordinates = initPointState(skyCoordinates, points)
		distance, breakExec = drawGrid(skyCoordinates, distance, points)
		//fmt.Println("new distance", distance)

		if breakExec {
			fmt.Println(second - 1)
			break
		}

		points = movePointsOneSecond(points)
	}
}

func movePointsOneSecond(points []Point) []Point {

	for i := 0; i < len(points); i++ {
		points[i].X += points[i].vX
		points[i].Y += points[i].vY
 	}

	return points
}

func turnBackOneSecond(points []Point) []Point {

	for i := 0; i < len(points); i++ {
		points[i].X -= points[i].vX
		points[i].Y -= points[i].vY
	}

	return points
}

func drawGrid(coordinates map[int64]map[int64]string, distance float64, currentSky []Point) (float64, bool) {
	var minX, maxX int64 = 0, 0
	var minY, maxY int64 = 0, 0
	var i, j int64

	for keyY, dimensionY := range coordinates {

		if keyY > maxY {
			maxY = keyY
		}

		if keyY < minY {
			minY = keyY
		}

		for keyX, _ := range dimensionY {
			if keyX > maxX {
				maxX = keyX
			}

			if keyX < minX {
				minX = keyX
			}
		}
	}

	newDistance := math.Sqrt(math.Pow(float64(maxX - minX), 2) + math.Pow(float64(maxY - minY), 2))
	fmt.Println("distance, newDistance, delta", distance, newDistance, distance - newDistance)

	breakExec := false
	if newDistance > distance && distance != 0 {
		coordinates = make(map[int64]map[int64]string, 0)

		// message -> AJZNXHKE
		currentSky = turnBackOneSecond(currentSky)
		coordinates = initPointState(coordinates, currentSky)


		var issetX, issetY bool
		breakExec = true

		for i = minY; i <= maxY; i++ {
			for j = minX; j <= maxX; j++ {

				_, issetY = coordinates[i]

				if issetY {
					_, issetX = coordinates[i][j]

					if !issetX {
						fmt.Print(".")
					} else {
						fmt.Print("#")
					}
				} else {
					fmt.Print(".")
				}
			}

			fmt.Println("")
		}
	}


	return newDistance, breakExec
}

func initPointState(coordinates map[int64]map[int64]string, points []Point) map[int64]map[int64]string {

	var X, Y int64
	var issetY, issetX bool

	for i := 0; i < len(points); i++ {
		X = points[i].X
		Y = points[i].Y

		_, issetY = coordinates[Y]

		if !issetY {
			coordinates[Y] = make(map[int64]string, int(X))
			coordinates[Y][X] = "#"
		} else {

			_, issetX = coordinates[Y][X]

			if !issetX {
				coordinates[Y][X] = "#"
			}
		}
	}

	return coordinates
}