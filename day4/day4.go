package day4

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"regexp"
	"sort"
	"strconv"
)

/*
	id
		date: shift array
*/

type Instructions [60]string

type SleepyGuard struct {
	id string
	minutesAsleep int
	maxSleepingMinute int
	theSleepingMinute int
}

func FindSleepingGuards() {
	//file := helpers.Fread("day4/test_input.txt")
	file := helpers.Fread("day4/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	days := make(map[string]Instructions)

	for scanner.Scan() {
		s := scanner.Text()

		initInstructionList(s, days)
	}

	// sort days
	fmt.Println("days", days)
	dates := make([]string, len(days))
	i := 0
	for date, _ := range days {
		dates[i] = date
		i++
	}
	sort.Strings(dates)

	guards := createGuards(days, dates)
	fmt.Println("guards", guards)

	theSleepyGuard := whoSleepsTheMost(guards)
	fmt.Println(theSleepyGuard)

	sleepyIdInt, _ := strconv.ParseInt(theSleepyGuard.id, 10, 64)
	fmt.Println("Strategy 1", sleepyIdInt*int64(theSleepyGuard.theSleepingMinute))

	theMostSleepingMinuteGuard := mostSleepingMinute(guards)
	fmt.Println("sleeps the most", theMostSleepingMinuteGuard)

	sleepyIdInt, _ = strconv.ParseInt(theMostSleepingMinuteGuard.id, 10, 64)
	fmt.Println("Strategy 2", sleepyIdInt * int64(theMostSleepingMinuteGuard.theSleepingMinute) )
}

func mostSleepingMinute(guards map[string][]int) SleepyGuard {
	theSleepyGuard := SleepyGuard{"", 0, 0, 0}

	sleepTime := 0
	sleepMinuteIndex := 0
	maxSleepOnMinute := 0
	guardId := ""

	for id, guard := range guards {
		for j := 0; j < len(guard); j++ {
			sleepTime += guard[j]

			if guard[j] > maxSleepOnMinute {
				maxSleepOnMinute = guard[j]
				sleepMinuteIndex = j
				guardId = id
			}
		}

		fmt.Println("most sleep", guardId, maxSleepOnMinute)

		if theSleepyGuard.maxSleepingMinute < maxSleepOnMinute {
			theSleepyGuard.id = guardId
			theSleepyGuard.maxSleepingMinute = maxSleepOnMinute
			theSleepyGuard.theSleepingMinute = sleepMinuteIndex
		}
	}

	return theSleepyGuard
}

func whoSleepsTheMost(guards map[string][]int) SleepyGuard {
	theSleepyGuard := SleepyGuard{"", 0, 0, 0}
	for id, guard := range guards {

		sleepTime := 0;
		sleepMinuteIndex := 0;
		maxSleepOnMinute := 0
		for j := 0; j < len(guard); j++ {
			sleepTime += guard[j]

			if guard[j] > maxSleepOnMinute {
				maxSleepOnMinute = guard[j]
				sleepMinuteIndex = j
			}
		}
		
		if theSleepyGuard.minutesAsleep < sleepTime {
			theSleepyGuard.id = id
			theSleepyGuard.minutesAsleep = sleepTime
			theSleepyGuard.theSleepingMinute = sleepMinuteIndex
		}
	}

	//fmt.Println("most sleep guard", theSleepyGuard)

	return theSleepyGuard
}

func createGuards(days map[string]Instructions, dates []string) map[string][]int {
	var guardId string
	var sleepStarts int
	var sleepEnds int

	guards := make(map[string][]int)


	//for _, instructions := range days {
	for _, date := range dates {
		instructions := days[date]

		for minutes, instruction := range instructions {

			switch instruction {
			case "":
				continue
			case "falls asleep":
				sleepStarts = minutes
			case "wakes up":
				sleepEnds = minutes

				for i := sleepStarts; i < sleepEnds; i++ {
					shifts := guards[guardId]

					shifts[i]++
					guards[guardId] = shifts
				}
			default:
				idMatch := regexp.MustCompile("#(\\d+)").FindAllStringSubmatch(instruction, -1)

				guardId = idMatch[0][1]

				if _, exists := guards[guardId]; !exists {
					guard := make([]int, 60)
					guards[guardId] = guard
				}
			}

		}
	}
	return guards
}

func initInstructionList(s string, days map[string]Instructions) {
	date, time, guardMove := getInstructions(s)
	minutes := getShiftTime(time)
	if instructions, defined := days[date]; defined {
		instructions[minutes] = guardMove
		days[date] = instructions
	} else {
		instructions := Instructions{}
		instructions[minutes] = guardMove
		days[date] = instructions
	}
}

func getShiftTime(time string) int64 {
	timeInMinutes := regexp.MustCompile(":([0-9]+)").FindAllStringSubmatch(time, -1)
	timeIndex, _ := strconv.ParseInt(timeInMinutes[0][1], 10, 64)

	return timeIndex
}

func getInstructions(s string) (string, string, string) {
	userParams := regexp.MustCompile("(\\d{4}-\\d{2}-\\d{2})\\s(\\d{2}:\\d{2})]\\s(.+)").FindAllStringSubmatch(s, -1)

	return userParams[0][1], userParams[0][2], userParams[0][3]
}