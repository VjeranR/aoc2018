package helpers

import (
	"log"
	"os"
)

func Fread(fileName string) *os.File {
	file, err := os.Open(fileName)

	if err != nil {
		log.Fatal(err)
	}

	return file
}
