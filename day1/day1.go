package day1

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"regexp"
	"strconv"
	"strings"
)

func Sum() {
	foundFrequency := false
	var frequencies = make(map[int64]string)
	frequencies[0] = "0"
	var sum int64 = 0
	i := 0

	for !foundFrequency {
		file := helpers.Fread("day1/test_input3.txt")
		defer file.Close()

		scanner := bufio.NewScanner(file)

		for scanner.Scan() {
			s := scanner.Text()

			sign, number := extractNumberAndSignFromString(s)

			switch sign {
			case "+":
				sum += number
			case "-":
				sum -= number
			}

			_, indicator := frequencies[sum]

			if indicator {
				fmt.Println("found it", sum)
				foundFrequency = true
				break
			} else {
				fmt.Println("setting frequency", sum)
				frequencies[sum] = s
			}
		}

		i++
	}

	fmt.Println("number of loops", i)
	fmt.Println(sum)
	fmt.Println(frequencies)
}

func extractNumberAndSignFromString(s string) (string, int64) {
	signArr := regexp.MustCompile("[\\d]").Split(s, -1)
	splittedArr := regexp.MustCompile("[+-]").Split(s, -1)

	sign := strings.TrimSpace(signArr[0])
	splitted := strings.TrimSpace(splittedArr[1])

	n, _ := strconv.ParseInt(splitted, 10, 64)

	return sign, n
}


