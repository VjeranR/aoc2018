package day5

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"strings"
)

func AlchemicalReduction() {
	//file := helpers.Fread("day5/test_input.txt")
	file := helpers.Fread("day5/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
	    formula := scanner.Text()
	    fmt.Println(formula)

		reactionsStillLeft := true
		for reactionsStillLeft {

			letters := strings.Split(formula, "")
			numberOfLetters := len(letters)

			reactionsStillLeft = false
			for i := 0; i < numberOfLetters; i++ {
				j := i + 1

				if j < numberOfLetters {
					if strings.EqualFold(letters[i], letters[j]) {
						l1 := []rune(letters[i])
						l2 := []rune(letters[j])


						if l1[0] != l2[0] {
							formula = strings.Replace(formula, letters[i]+letters[j], "", 1)
							reactionsStillLeft = true
						}

					}
				}
			}
		}

		fmt.Println(formula)
		fmt.Println("Solution 1", len(formula))
	}
}

type ProblematicElement struct {
	id string
	length int
}

func AlchemicalReduction2() {
	//file := helpers.Fread("day5/test_input.txt")
	file := helpers.Fread("day5/input.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)
	problematicElement := ProblematicElement{"", 50000}

	for scanner.Scan() {
		formula := scanner.Text()
		fmt.Println(formula)

		alphabet := strings.Split("abcdefghijklmnopqrstuvwxyz", "")

		for _, letter := range alphabet {

			changedFormula := strings.Replace(formula, letter, "", -1)
			changedFormula = strings.Replace(changedFormula, strings.ToUpper(letter), "", -1)

			reactionsStillLeft := true
			for reactionsStillLeft {

				letters := strings.Split(changedFormula, "")
				numberOfLetters := len(letters)

				reactionsStillLeft = false
				for i := 0; i < numberOfLetters; i++ {
					j := i + 1

					if j < numberOfLetters {
						if strings.EqualFold(letters[i], letters[j]) {
							l1 := []rune(letters[i])
							l2 := []rune(letters[j])


							if l1[0] != l2[0] {
								changedFormula = strings.Replace(changedFormula, letters[i]+letters[j], "", 1)
								reactionsStillLeft = true
							}

						}
					}
				}
			}

			reducedFormulaength := len(changedFormula)
			if reducedFormulaength < problematicElement.length {
				problematicElement.id = letter
				problematicElement.length = reducedFormulaength
			}
		}

		fmt.Println("Solution 2", problematicElement)

	}
}