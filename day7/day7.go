package day7

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"regexp"
	"sort"
	"strings"
)

type Step struct {
	id       string
	children []*Step
	visited  bool
	order    []string
	prerequisites string
}

func AssembleDevice() {
	//file := helpers.Fread("day7/test_input.txt")
	file := helpers.Fread("day7/input2.txt")
	defer file.Close()
	scanner := bufio.NewScanner(file)

	steps := make(map[string]*Step, 0)

	for scanner.Scan() {
	    s := scanner.Text()

		initializeGraph(s, steps)
	}

	for _, step := range steps {

		for _, child := range step.children {
			step.order = append(step.order, child.id)
			child.prerequisites += step.id
		}

	}

	// find start node
	startNodes := findStartNode(steps)
	sort.Strings(startNodes)

	fmt.Println("final start nodes", startNodes)

	solution := ""
	for _, startNode := range startNodes {
		solution += assembleBFS(steps[startNode])
		fmt.Println("solution", solution)
	}
}

func findStartNode(steps map[string]*Step) []string {
	startNodes := make([]string, 0)
	for _, step := range steps {

		if len(step.children) > 0 {
			startNodes = append(startNodes, step.id)
		}
	}
	for _, step := range steps {

		for _, child := range step.children {

			toDelete := make([]int, 0)
			for i := 0; i < len(startNodes); i++ {
				if child.id == startNodes[i] {
					toDelete = append(toDelete, i)
				}
			}

			for _, index := range toDelete {
				startNodes = append(startNodes[:index], startNodes[index+1:]...)
			}
		}
	}
	return startNodes
}

func assembleBFS(startNode *Step) string{
	queue := make([]*Step, 0)
	queue = append(queue, startNode)
	startNode.visited = true
	order := ""

	for len(queue) > 0 {
		v := queue[0]
		order += v.id

		if len(queue) == 1 {
			queue = make([]*Step, 0)
		} else {
			queue = queue[1:]
		}


		for i := 0; i < len(v.order); i++ {
			child := v.children[i]

			prerequisitesStillProcessing := prerequisitesStillProcessing(queue, child)

			// if child has prerequisites still processing in queue
			if child.visited == false && !prerequisitesStillProcessing {
				queue = append(queue, child)
				child.visited = true
			}
		}

		n := len(queue)
		notSwapped := true

		for notSwapped {
			notSwapped = false

			for i := 1; i < n; i++ {

				if queue[i-1].id > queue[i].id {
					queue = swap(queue[:n], i)
					notSwapped = true
				}
			}
		}
	}

	return order
}

func prerequisitesStillProcessing(queue []*Step, child *Step) bool {
	prerequisitesStillProcessing := false

	for _, node := range queue {
		if strings.Contains(child.prerequisites, node.id) {
			prerequisitesStillProcessing = true
		}
	}

	return prerequisitesStillProcessing
}

func swap(queue []*Step, i int) []*Step{
	temp := queue[i-1]
	queue[i-1] = queue[i]
	queue[i] = temp

	return queue
}

func initializeGraph(s string, steps map[string]*Step) {
	userParams := regexp.MustCompile("Step (\\w) must be finished before step (\\w) can begin").FindAllStringSubmatch(s, -1)

	id := userParams[0][1]
	child := userParams[0][2]

	if step, initialized := steps[id]; !initialized {
		step = &Step{id, make([]*Step, 0), false, make([]string, 0), ""}

		initializeGraphNodes(steps, child, step, id)
	} else {
		initializeGraphNodes(steps, child, step, id)
	}
}

func initializeGraphNodes(steps map[string]*Step, child string, step *Step, id string) {

	if node, nodeExists := steps[child]; nodeExists {
		step.children = append(step.children, node)
	} else {
		newStep := Step{child, make([]*Step, 0), false, make([]string, 0), ""}

		step.children = append(step.children, &newStep)

		steps[child] = &newStep
	}

	steps[id] = step
}