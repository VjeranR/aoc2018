package day13

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"strings"
)

type MiningCart struct {
	x, y         int
	direction    string
	prevX, prevY int
	//intersectionMemory int
}

func MineCartMadness() {
	file := helpers.Fread("day13/test_input_circular.txt")
	//file := helpers.Fread("day13/input.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)

	undergroundMap := make([][]string, 0)
	mapWithoutCarts := make([][]string, 0)
	carts := make([]MiningCart, 0)
	for scanner.Scan() {
	    s := scanner.Text()

	    mapRow := strings.Split(s, "")

	    rowMap := make([]string, 0)
	    rowWithoutCarts := make([]string, 0)
		for _, char := range mapRow  {
			rowMap = append(rowMap, char)

			if char == "^" || char == "v" {
				rowWithoutCarts = append(rowWithoutCarts, "|")
				carts = append(carts, MiningCart{})
			} else if char == "<" || char == ">" {
				rowWithoutCarts = append(rowWithoutCarts, "-")
			} else {
				rowWithoutCarts = append(rowWithoutCarts, char)
			}

		}
		undergroundMap = append(undergroundMap, rowMap)
		mapWithoutCarts = append(mapWithoutCarts, rowWithoutCarts)
	}

	/* print what you constructed */
	fmt.Println("")

	fmt.Println(mapWithoutCarts)
	/* ------------------------------------ */

	fmt.Println("--------------------")

	crashed := false

	for crashed != true {

		moves := make([]MiningCart, 0)
		for y := 0; y < len(undergroundMap); y++ {

			for x := 0; x < len(undergroundMap[y]); x++ {
				coordinate := undergroundMap[y][x]

				switch coordinate {
				case ">":
					if x+1 < len(undergroundMap[y]) {
						nextStep := undergroundMap[y][x+1]
						direction := ">"

						if nextStep == `/` {
							direction = "^"
						} else if nextStep == `\` {
							direction = "v"
						}

						moves = append(moves, MiningCart{x+1, y, direction, x, y})
					}
				case "v":
					if y+1 < len(undergroundMap) {
						nextStep := undergroundMap[y+1][x]
						direction := "v"

						if nextStep == `/` {
							direction = "<"
						} else if nextStep == `\` {
							direction = ">"
						}

						moves = append(moves, MiningCart{x, y+1, direction, x, y})
					}
				case "<":
					if x-1 >= 0 {
						nextStep := undergroundMap[y][x-1]
						direction := "<"

						if nextStep == `/` {
							direction = "v"
						} else if nextStep == `\` {
							direction = "^"
						}

						moves = append(moves, MiningCart{x-1, y, direction, x, y})
					}
				case "^":
					if y-1 >= 0 {
						nextStep := undergroundMap[y-1][x]
						direction := "^"

						if nextStep == `/` {
							direction = ">"
						} else if nextStep == `\` {
							direction = "<"
						}

						moves = append(moves, MiningCart{x, y-1, direction, x, y})
					}
				}

				fmt.Println(mapWithoutCarts)
			}
		}

		// move. if crashed
		for len(moves) > 0 {
			move := moves[len(moves) - 1]

			crashed = checkCrash(undergroundMap[move.y][move.x])
			if crashed {
				fmt.Println(move.x, move.y)
				break
			}

			undergroundMap[move.y][move.x] = move.direction
			undergroundMap[move.prevY][move.prevX] = mapWithoutCarts[move.prevY][move.prevX]
			moves = moves[:len(moves) - 1]
		}

	}
}

func checkCrash(previousSymbol string) bool {
	cartSymbols := []string{"v", "^", ">", "<"}

	for _, cartSymbol := range cartSymbols {
		if previousSymbol == cartSymbol {
			return true
		}

	}

	return false
}