package day9

import (
	"aoc2018/helpers"
	"bufio"
	"fmt"
	"regexp"
	"strconv"
)

type CurrentElement struct {
	previous *CurrentElement
	next *CurrentElement
	value int
}

func MarbleGame() {
	//file := helpers.Fread("day9/test_input.txt")
	file := helpers.Fread("day9/input.txt")
	defer file.Close()

	scanner := bufio.NewScanner(file)

	for scanner.Scan() {
		s := scanner.Text()
		userParams := regexp.MustCompile("(\\d+) players; last marble is worth (\\d+) points").FindAllStringSubmatch(s, -1)

		numberOfPlayers, _ := strconv.ParseInt(userParams[0][1], 10, 64)
		maxValue, _ := strconv.ParseInt(userParams[0][2], 10, 64)

		fmt.Println(numberOfPlayers, maxValue)

		players := make([]int, numberOfPlayers)

		first := CurrentElement{}
		first.previous = &first
		first.next = &first
		first.value = 0

		current := first
		player := 0
		for i := 1; i <= int(maxValue); i++ {

			if i % 23 == 0 {
				players[player] += i

				for j := 0; j < 7; j++ {
					current = *current.previous
				}

				players[player] += current.value

				// remove marble
				newCurrent := current.next
				newCurrent.previous = current.previous

				newPrevious := current.previous
				newPrevious.next = newCurrent

				current = *newCurrent
			} else {
				newPrevious := current.next
				newNext := current.next.next


				newMarble := CurrentElement{newPrevious, newNext, i}
				newPrevious.next = &newMarble
				newNext.previous = &newMarble

				current = newMarble
			}


			//printGameState(first, i, players, player)
			player = advancePlayerCounter(player, numberOfPlayers)
		}

		//fmt.Println("----------------------")

		highScore := 0
		winner := 0
		for player, score := range players {
			if score > highScore {
				highScore = score
				winner = player + 1
			}
		}

		fmt.Println("winner", winner, highScore)
		fmt.Println("----------------------")
	}

}

func advancePlayerCounter(player int, numberOfPlayers int64) int {
	player++
	if player >= int(numberOfPlayers) {
		player -= int(numberOfPlayers)
	}

	return player
}

func printGameState(first CurrentElement, i int, players []int, currentPlayer int) {
	node := &first

	fmt.Print("[", currentPlayer + 1, "] ")
	for j := 0; j < i + 1; j++ {

		if node.value == i {
			fmt.Print("(", node.value, ")", " ")
		} else {
			fmt.Print(node.value, " ")
		}
		node = node.next
	}
	fmt.Println("--", players)
}